library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity nexys_top is
  port(
    --Clocks
    clk : in std_ulogic;
    GTP_CLK_N : in std_ulogic;
    GTP_CLK_P : in std_ulogic;
    FMC_MGT_CLK_N : in std_ulogic;
    FMC_MGT_CLK_P : in std_ulogic;
    --LEDs
    led : out std_ulogic_vector(7 downto 0);
    --Push Buttons
    btnc : in std_ulogic;
    btnd : in std_ulogic;
    btnl : in std_ulogic;
    btnu : in std_ulogic;
    btnr : in std_ulogic;
    cpu_resetn : in std_ulogic;
    --Slide Switches
    sw : in std_ulogic_vector(7 downto 0);
    --OLED Display
    oled_dc : out std_ulogic;
    oled_res : out std_ulogic;
    oled_sclk : out std_ulogic;
    oled_sdin : out std_ulogic;
    oled_vbat : out std_ulogic;
    oled_vdd : out std_ulogic;
    --HDMI Input
    hdmi_rx_cec : inout std_ulogic;
    hdmi_rx_clk_n : in std_ulogic;
    hdmi_rx_clk_p : in std_ulogic;
    hdmi_rx_hpa : out std_ulogic;
    hdmi_rx_scl : inout std_ulogic;
    hdmi_rx_sda : inout std_ulogic;
    hdmi_rx_txen : out std_ulogic;
    hdmi_rx_n : in std_ulogic_vector(2 downto 0);
    hdmi_rx_p : in std_ulogic_vector(2 downto 0);
    --HDMI Output
    hdmi_tx_cec : inout std_ulogic;
    hdmi_tx_clk_n : out std_ulogic;
    hdmi_tx_clk_p : out std_ulogic;
    hdmi_tx_hpd : in std_ulogic;
    hdmi_tx_rscl : inout std_ulogic;
    hdmi_tx_rsda : inout std_ulogic;
    hdmi_tx_n : out std_ulogic_vector(2 downto 0);
    hdmi_tx_p : out std_ulogic_vector(2 downto 0);
    --Display Port
    dp_tx_aux_n : out std_ulogic;
    dp_tx_aux_p : out std_ulogic;
    dp_rx_aux_n : in std_ulogic;
    dp_rx_aux_p : in std_ulogic;
    dp_tx_hpd : in std_ulogic;
    --Audio Codec
    ac_adc_sdata : in std_ulogic; --Datasheet wrong
    ac_bclk : out std_ulogic;
    ac_dac_sdata : out std_ulogic; --Datasheet wrong
    ac_lrclk : out std_ulogic;
    ac_mclk : out std_ulogic;
    --PMOD headers
    ja : inout std_ulogic_vector(7 downto 0);
    jb : inout std_ulogic_vector(7 downto 0);
    jc : inout std_ulogic_vector(7 downto 0);
    --XADC header
    xa_p : in std_ulogic_vector(7 downto 0);
    xa_n : in std_ulogic_vector(7 downto 0);
    --UART over USB
    uart_rx_out : out std_ulogic;
    uart_tx_in : in std_ulogic;
    --Ethernet
    eth_int_b : in std_ulogic;
    eth_mdc : out std_ulogic;
    eth_mdio : inout std_ulogic;
    eth_pme_b : inout std_ulogic; --Not sure on direction
    eth_rst_b : out std_ulogic;
    eth_rxck : in std_ulogic;
    eth_rxctl : in std_ulogic;
    etc_rxd : in std_ulogic_vector(7 downto 0);
    eth_txck : out std_ulogic;
    eth_txctl : out std_ulogic;
    eth_txd : out std_ulogic_vector(7 downto 0);
    --Fan PWM (Not attached)
    fan_pwm : out std_ulogic;
    --DPTI/DSPI (USB Interface)
    prog_clko : in std_ulogic;
    prog_d : inout std_ulogic_vector(7 downto 0);
    prog_oen : out std_ulogic;
    prog_rdn : out std_ulogic;
    prog_wrn : out std_ulogic;
    prog_rxen : in std_ulogic;
    prog_txen : in std_ulogic;
    prog_siwun : out std_ulogic;
    prog_spien : in std_ulogic;
    --HID Port (Kb / Mouse only)
    ps2_clk : inout std_ulogic;
    ps2_data : inout std_ulogic;
    --QSPI (Flash)
    qspi_cs : out std_ulogic;
    qspi_dq : inout std_ulogic_vector(3 downto 0);
    qspi_cclk : out std_ulogic;
    --SD Card (QSPI-esque)
    sd_cclk : inout std_ulogic;
    sd_cd : in std_ulogic;
    sd_cmd : inout std_ulogic;
    sd_d : inout std_ulogic_vector(3 downto 0);
    sd_reset : out std_ulogic;
    --I2C
    scl : out std_ulogic;
    sda : inout std_ulogic;
    --Voltage Adjustment
    set_vadj : out std_ulogic_vector(1 downto 0);
    vadj_en : out std_ulogic;
    --FMC
    --Pins 0, 1, 17 and 18 are clock-capable (input and output)
    fmc_clk_m2c_n : out std_ulogic_vector(1 downto 0);
    fmc_clk_m2c_p : out std_ulogic_vector(1 downto 0);
    fmc_la_p : inout std_ulogic_vector(33 downto 0);
    fmc_la_n : inout std_ulogic_vector(33 downto 0);
    );

end entity;

module nexys_top(
				 //Input Clocks
				 //sysclk, 100MHz MEMS
				 input wire 	   clk;
				 //FMC Transciever Clocks, External, 156.25MHz by default, provided by add-in card
				 input wire 	   GTP_CLK_N;
				 input wire 	   GTP_CLK_P;
				 input wire 	   FMC_MGT_CLK_N;
				 input wire 	   FMC_MGT_CLK_P;
				 //LEDs
				 output wire [7:0] led;
				 //Push Buttons
				 input wire 	   btnc; //Center
				 input wire 	   btnd; //Down
				 input wire 	   btnl; //Left
				 input wire 	   btnu; //Up
				 input wire 	   btnr; //Right
				 input wire 	   cpu_resetn; //CPU Reset Button
				 //Slide switches
				 input wire [7:0]  sw;
				 //OLED display
				 output wire 	   oled_dc;
				 output wire 	   oled_res;
				 output wire 	   oled_sclk; //SPI clock
				 output wire 	   oled_sdin; //SPI data -> OLED
				 output wire 	   oled_vbat;
				 output wire 	   oled_vdd;
				 //HDMI input
				 inout wire 	   hdmi_rx_cec;
				 input wire 	   hdmi_rx_clk_n;
				 input wire 	   hdmi_rx_clk_p;
				 output wire 	   hdmi_rx_hpa;
				 inout wire 	   hdmi_rx_scl; //I2C Clock
				 inout wire 	   hdmi_rx_sda; //I2C Data
				 output wire 	   hdmi_rx_txen;
				 input wire [2:0]  hdmi_rx_n;
				 input wire [2:0]  hdmi_rx_p;
				 //HDMI output
				 inout wire 	   hdmi_tx_cec;
				 output wire 	   hdmi_tx_clk_n;
				 output wire 	   hdmi_tx_clk_p;
				 input wire 	   hdmi_tx_hpd;
				 inout wire 	   hdmi_tx_rscl; //I2C Clock
				 inout wire 	   hdmi_tx_rsda; //I2C Data
				 output wire [2:0] hdmi_tx_n;
				 output wire [2:0] hdmi_tx_p;
				 //Display Port
				 output wire 	   dp_tx_aux_n;
				 output wire 	   dp_tx_aux_p;
				 input wire 	   dp_rx_aux_n;
				 input wire 	   dp_rx_aux_p;
				 input wire 	   dp_tx_hpd;
				 //Audio Codec
				 input wire 	   ac_adc_sdata; //Datasheet is wrong
				 output wire 	   ac_bclk;
				 output wire 	   ac_dac_sdata; //Datasheet is wrong
				 output wire 	   ac_lrclk;
				 output wire 	   ac_mclk;
				 //PMOD headers
				 inout wire [7:0]  ja;
				 inout wire [7:0]  jb;
				 inout wire [7:0]  jc;
				 //XADC header
				 input wire [3:0]  xa_p;
				 input wire [3:0]  xa_n;
				 //UART USB port
				 output wire 	   uart_rx_out;
				 input wire 	   uart_tx_in;
				 //Ethernet
				 input wire 	   eth_int_b;
				 output wire 	   eth_mdc;
				 inout wire 	   eth_mdio;
				 inout wire 	   eth_pme_b; //Not sure on direction
				 output wire 	   eth_rst_b;
				 input wire 	   eth_rxck;
				 input wire 	   eth_rxctl;
				 input wire [3:0]  eth_rxd;
				 output wire 	   eth_txck;
				 output wire 	   eth_txctl;
				 output wire [3:0] eth_txd;
				 //Fan PWM (Not attached)
				 output wire 	   fan_pwm;
				 //DPTI/DSPI (USB interface)
				 input wire 	   prog_clko;
				 inout wire 	   prog_d[7:0];
				 output wire 	   prog_oen;
				 output wire 	   prog_rdn;
				 output wire 	   prog_wrn;
				 input wire 	   prog_rxen;
				 input wire 	   prog_txen;
				 output wire 	   prog_siwun;
				 input wire 	   prog_spien;
				 //HID Port (KB or Mouse only)
				 inout wire 	   ps2_clk;
				 inout wire 	   ps2_data;
				 //QSPI (Flash)
				 output wire 	   qspi_cs;
				 inout wire [3:0]  qspi_dq;
				 output wire 	   qspi_cclk;
				 //SD Card (QSPI-esque)
				 inout wire 	   sd_cclk; //Clock
				 input wire 	   sd_cd; //Card Detect, Active-low
				 inout wire 	   sd_cmd; //Command
				 inout wire [3:0]  sd_d; //Data
				 output wire 	   sd_reset; //Reset, Active-high
				 //I2C
				 output wire 	   scl;
				 inout wire 	   sda;
				 //Voltage Adjustment
				 output wire [1:0] set_vadj;
				 output wire 	   vadj_en;
				 //FMC
				 //Pins 0, 1, 17 and 18 are clock-capable (input and output).
				 output wire [1:0] fmc_clk_m2c_n;
				 output wire [1:0] fmc_clk_m2c_p;
				 inout wire [33:9] fmc_la_p;
				 inout wire [33:0] fmc_la_n;
				 );

endmodule; // nexys
